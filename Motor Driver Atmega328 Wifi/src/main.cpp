#include <Arduino.h>
#include <SoftwareSerial.h>

#define MOTOR_PIN 3
#define BUTTON_PIN A0

SoftwareSerial espSerial(5, 6);
String str;

void setup() {
  pinMode(BUTTON_PIN, INPUT);
  pinMode(MOTOR_PIN, OUTPUT);
  Serial.begin(115200);
  espSerial.begin(115200);
}

void loop() {

  int value_button = analogRead(BUTTON_PIN);
  str = String(value_button);

    if (value_button > 400 && value_button < 600) {
    Serial.println("Button1 " + str);
    analogWrite(MOTOR_PIN, 255);
  } else if (value_button > 300 && value_button < 400) {
    Serial.println("Button2 " + str);
    analogWrite(MOTOR_PIN, 200);
  } else if (value_button > 200 && value_button < 300) {
    Serial.println("Button3 " + str);
    analogWrite(MOTOR_PIN, 130);
  } else if (value_button > 0 && value_button < 200) {
    Serial.println("Button4 " + str);;
    analogWrite(MOTOR_PIN, 65);
  }
  espSerial.println(str);
  delay(1000);
}